package com.epam.controller;

import com.epam.model.LongestPlataeu;
import com.epam.model.Model;

/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    Model link;

    @Override
    public String findElements() {
        link=new LongestPlataeu();
        return link.findElements();
    }
}
