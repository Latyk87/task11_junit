package com.epam.model;

/**
 * Class implements a algorithm for longest sequence.
 * Created by Borys Latyk on 05/12/2019.
 *
 * @version 2.1
 * @since 06.12.2019
 */
public class LongestPlataeu implements Model {

    private static int[] plataeu = {2, 3, 4, 5, 6, 7, 10, 10, 10, 5, 6, 6, 7, 8, 2, 4, 5, 6, 5, 7, 7, 7, 7};

    public String findElements() {

        for (int i = 0; i < plataeu.length; i++) {
            System.out.print(plataeu[i] + " ");
        }
        System.out.println();

        int inp_length = plataeu.length;
        int len_big_pt = 0;
        int big_p1 = 0;
        int big_p2 = 0;
        //if platuea exists it can start anywhere between 0 and length-3. Can't exist after that
        for (int p1 = 0; p1 < inp_length - 3; p1++) {
            //Plataue cant start at the first 3 locations
            for (int p2 = p1 + 3; p2 < inp_length; p2++) {
                boolean condition1 = false;
                boolean condition2 = true;
                //value at the end of the plateau can be less or equal to value at the begining .
                if (plataeu[p2] <= plataeu[p1]) {
                    condition1 = true;
                    //all middle values equal and higher than 1st value
                    for (int p = p1 + 2; p < p2; p++) {

                        condition2 = true;
                        if ((plataeu[p] != plataeu[p - 1] || plataeu[p] <= plataeu[p1])) {
                            condition2 = false;

                            break;
                        }
                    }
                }
                if (condition1 && condition2) {
                    int pt_len = p2 - p1;
                    if (pt_len >= len_big_pt) {
                        len_big_pt = pt_len;
                        big_p1 = p1;
                        big_p2 = p2;
                    }
                }
            }
        }
        if (len_big_pt > 0) {
            return ("Biggest plateau exists from index " + big_p1 + " to index " + big_p2);
        } else {
            return ("No plateau");
        }
    }

    public static int[] getPlataeu() {
        return plataeu;
    }
}
