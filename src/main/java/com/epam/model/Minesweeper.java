package com.epam.model;

import java.util.Random;
import java.util.Scanner;
/**
 * Class creates a a little console game.
 * Created by Borys Latyk on 05/12/2019.
 *
 * @version 2.1
 * @since 06.12.2019
 */
public class Minesweeper {

   static int mine; //integer for mines

    public static void prepareGame() {
         int ROW; //integer for rows
         int COL; //integer for columns

        Scanner scan = new Scanner(System.in); //creating scan for user input
        //getting user input for rows and columns
        try {
            System.out.println("Enter Number of Rows: ");
            ROW = scan.nextInt();
            System.out.println("Enter Number of Columns: ");
            COL = scan.nextInt();
            mine = (ROW * COL) / 6; //setting the number of mines 1/6 of the rows and columns
            startGame(ROW,COL);

        }catch (Exception e) {
            System.err.println("Reading INT failed, next time enter an integer!");
            System.exit(0);
        }
    }
public static int [][] startGame(int rw, int cl){
      int[][] board = new int[rw][cl];//creating grid array with index of rows and columns
        Random rand = new Random(); //creating random number generator
            int count = 0; //counter
            int a; //random number for rows
            int b; //random number for columns
            //while loop that creates the mines
            while (count < mine) {
                a = rand.nextInt(rw);
                b = rand.nextInt(cl);
                if (board[a][b] != 9) //while not a mine (value = 9) change it to mine
                {
                    board[a][b] = 9;
                    count++;//add to counter if mine is there
                }
            }

            //for loop to create grid and fill
            for (int r = 0; r < board.length; r++) {
                for (int c = 0; c < board[r].length; c++) {
                    //created to check to make sure array isn't out of bounds
                    int rMax = r + 1; //maxium amount of r at each point
                    int cMax = c + 1; //maxium amount of columns at each point
                    int rMin = r - 1;//minimum amount of rows at each point
                    int cMin = c - 1;//minimum amount of rows at each point
                    int count1 = 0;//another count variable

                    if (r == 0) {
                        rMin = 0; //setting minimum number of rows to 0 if r is 0
                    }
                    if (c == 0) {
                        cMin = 0; //setting minimum number of columns to 0 if c is 0
                    }
                    if (r == rw - 1) {
                        rMax = rw - 1; //setting max number of rows to user input rows - 1 (so the counter will not go past that point
                    }
                    if (c == cl - 1) {
                        cMax = cl - 1;//setting max number of columns to user input cols - 1 (so the counter will not go past that point
                    }
                    //to find the positions where not equal to 9(mine)
                    if (board[r][c] != 9) {
                        count1 = 0;//setting counter equal to 0
                        //for loop to scan the grid
                        for (int i = rMin; i <= rMax; i++) {
                            for (int j = cMin; j <= cMax; j++) {
                                if (board[i][j] == 9) {
                                    count1++; //if posistion is equal to 9 then add 1 to counter
                                }
                                if (count > 0) {
                                    board[r][c] = count1; //if count is greater than 0 than set the position to the ammount of the counter
                                }
                            }
                        }
                    }
                    //print out game grid
                    System.out.print(board[r][c] + " ");
                }
                System.out.println();
            }
    return board;
    }

    }
