package com.epam;

import com.epam.model.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;


public class MinesweeperTest {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @BeforeAll
    static void initAll() {
        logger1.info("Run before all methods");
    }

    @BeforeEach
    void init() {
        logger1.info("run before each method");
    }

    @DisplayName("Testing the Array")
    @Test
    public void testStartgame(){
        int a=2;
        int b=2;
        int [][] arrayResult=Minesweeper.startGame(a,b);
        int[][] arrayExpectedResult={{0,0},{0,0}};
        assertArrayEquals(arrayExpectedResult,arrayResult);
    }

    @AfterEach
    void closeTest() {
        logger1.info("Run after each method");
    }

    @AfterAll
    static void closeAll() {
        logger1.info("Run only once after all tests");
    }
}