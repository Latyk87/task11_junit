package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

public class JunitTestExample {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @BeforeAll
    static void initAll() {
        logger1.info("Run before all methods");
    }

    @BeforeEach
    void init() {
        logger1.info("run before each method");
    }

    @DisplayName("First test")

    @Test
    void test1() {
        logger1.info("Test method1");
    }

    @Test
    void test2() {
        logger1.info("Test method2");
    }

    @AfterEach
    void closeTest() {
        logger1.info("Run after each method");
    }

    @AfterAll
    static void closeAll() {
        logger1.info("Run only once after all tests");
    }
}
