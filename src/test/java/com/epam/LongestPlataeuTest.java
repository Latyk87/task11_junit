package com.epam;

import com.epam.model.LongestPlataeu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LongestPlataeuTest {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @BeforeAll
    static void initAll() {
        logger1.info("Run before all methods");
    }

    @BeforeEach
    void init() {
        logger1.info("run before each method");
    }

    @DisplayName("Testing the return value of method")
    @Test
    public void testfindElements() {
        LongestPlataeu longestPlataeu = new LongestPlataeu();
        String s = longestPlataeu.findElements();
        assertEquals(longestPlataeu.findElements(), s);
    }

    @DisplayName("Testing the main method variable")
    @Test
    public void test2findElements() {
        int[] check = LongestPlataeu.getPlataeu();
        assertNotNull(check);
    }

    @DisplayName("Testing the length of main method variable")
    @RepeatedTest(3)
    public void test3findElements() {
        int b = LongestPlataeu.getPlataeu().length;
        assertEquals(23, b);
    }

    @AfterEach
    void closeTest() {
        logger1.info("Run after each method");
    }

    @AfterAll
    static void closeAll() {
        logger1.info("Run only once after all tests");
    }
}
